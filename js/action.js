                 // это созданный объект, объекты имеют поля или свойства
                                 // они представляют из себя ключ:значение, т.е. название поля и значение его
   
const tasks = [
    
    {
    isComplete: false,
    id:1,
    name: 'Купить хлеб'  
    },
    
    {
    isComplete: true,
    id:2,
    name: 'Сделать уборку'
    },
    
    {
    isComplete: false,
    id:3,
    name: 'Приготовить еду'
    },
    
    {
    isComplete: true,
    id:4,
    name: 'Покушать'
    },
    
    {
    isComplete: false,
    id:5,
    name: 'Лечь спать'
    }
];


// function drawTask(task) {
//     return `
//         <div class="task">
//             <input type="checkbox" class="task__complite" ${task.isComplete ? 'checked' : ''} ></input> 
//             <span class="task__number">${task.id}</span>
//             <span class="task__name">${task.name}</span> 
//         </div>`; // task
// }

 const list = document.querySelector('.container__list');
// // tasks.forEach(item => { list.innerHTML += drawTask(item);
// // });


// console.log(tasks);


//-------------------- true way---------------------

function createTaskTag(task) {
    const name = document.createElement('span');  // в переменной name лежит пустой <span></span> 
    name.className = 'task__name'; // в name уже есть <span class="task_name"></span>
    name.innerText = task.name; // в name уже есть <span class="task_name">Имя_Таска_из_массива</span>

    const number = document.createElement('span');
    number.className = 'task__number';
    number.innerText = task.id;

    const button = document.createElement('input');
    button.type = 'checkbox';
    button.className = 'task__complite';
    button.checked = task.isComplete;

    const taskTag = document.createElement('div');
    taskTag.className = 'task'; // taskTag = <div class="task"></div>
    taskTag.appendChild(button);  // <div class="task"> <input></div>
    taskTag.appendChild(number); // <div class="task"> <input> <span class="number></span></div>
    taskTag.appendChild(name);  // <div class="task"> <input> <span class="number></span> <span class= "name></span></div>

    return taskTag;
}

tasks.forEach( item => {
    const myTask = createTaskTag(item);
    list.appendChild(myTask);
});